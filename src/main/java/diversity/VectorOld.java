package diversity;

import classes.Util;
import models.InterestProfile;
import net.sf.javaml.clustering.mcl.SparseVector;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.DefaultDataset;
import net.sf.javaml.core.DenseInstance;
import net.sf.javaml.core.Instance;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.*;

import static classes.Util.buildQuery;
import static classes.Util.INDEX_PATH;
import static classes.Util.createSearcher;
import static classes.Util.getSearchDocs;

public class VectorOld {

    private IndexSearcher searcher;
    private IndexReader reader;
    private List<InterestProfile> profiles;
    private int numDoc;
    private Map<String, Integer> allTerms;
    private String indexName;
    private Analyzer analyzer;

    public VectorOld(String indexName, Analyzer analyzer) {
        this.indexName = indexName;
        this.analyzer = analyzer;
        searcher = createSearcher(INDEX_PATH + indexName, new BM25Similarity());
        reader = searcher.getIndexReader();
        profiles = Util.getProfiles();
        numDoc = reader.numDocs();
    }



    private Dataset getDataset(KMeansSimilarity ks, List<SparseVector> vectors) {
        int totalTerms = ks.getNumTerms();
        List<Instance> instances = new ArrayList<>();


        for (int i = 0; i < vectors.size(); i++) {
            SparseVector vec = vectors.get(i);
            double[] dense = vec.getDense();
            Instance instance = new DenseInstance(dense);
            instances.add(instance);
        }

        Dataset data = new DefaultDataset();
        for (int i = 0; i < instances.size(); i++) {
            data.add(instances.get(i));
        }

        return data;
    }

    public void compareProfileDocs() {
        List<List<Map<Integer, Double>>> listOflist = new ArrayList<>();
        for (InterestProfile profile : profiles) {
            List<Map<Integer, Double>> listOfSimilarities = new ArrayList<>();
            Query query = buildQuery(profile.getTitle(), analyzer);
            ScoreDoc[] hits = getSearchDocs(query, searcher);
            for (int i = 0; i < hits.length - 1; i++) {
                listOfSimilarities.add(compareDoc(hits[i].doc, hits));
            }
            return;
        }
    }

    public Map<Integer, Double> compareDoc(int docId, ScoreDoc[] hits) {
        Map<Integer, Double> similarities = new HashMap<>();
        double similarity;

        for (int i = 0; i < hits.length - 1; i++) {
            int other = hits[i].doc;
            similarity = cosineSimilarity(docId, other);
            similarities.put(other, similarity);
        }
        return similarities;
    }

    private static double dotProduct(List<Double> array1, List<Double> array2) {
        assert array1.size() == array2.size();
        double dot = 0;
        for (int i = 0; i < array1.size() - 1; i++) {
            dot += array1.get(i) * array2.get(i);
        }
        return dot;
    }

    private double cosineSimilarity(int doc1, int doc2) {
        Map<String, Integer> freq1 = getDocTermVector(reader, doc1);
        Map<String, Integer> freq2 = getDocTermVector(reader, doc2);

        Map<String, Integer> combined = new HashMap<>();
        combined.putAll(freq1);
        combined.putAll(freq2);

        DocVectorOld docVec = new DocVectorOld(combined);
        Map<String, Integer> vec1 = docVec.getVector(freq1);
        Map<String, Integer> vec2 = docVec.getVector(freq2);
        return docVec.computeVectors(vec1,vec2);

    }

    private List<Double> calcDocTfIdf(int docId) {
        List<Double> tfIdfList = new ArrayList<>();
        try {
            Map<String, Integer> frequencies = getDocTermVector(reader, docId);

            for (String term : frequencies.keySet()) {
                int freqInDoc   = reader.docFreq(new Term("text", term));
                double idf      = Math.log(numDoc / freqInDoc);
                int tf          = frequencies.get(term);
                tfIdfList.add(tf * idf);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return tfIdfList;
    }

    private double vectorLenght(List<Double> list) {
        double vecLen = 0;
        for (int i = 0; i < list.size() - 1; i++) {
            vecLen += Math.pow(list.get(i), 2);
        }
        return Math.sqrt(vecLen);
    }

    private static Map<String, Integer> getDocTermVector(IndexReader reader, int docId) {
        Map<String, Integer> frequencies = new HashMap<>();
        try {
            Terms termFreqVector = reader.getTermVector(docId, "textVector");
            if(termFreqVector == null){
                //System.out.println("no term vector: docId = " + docId);
                return frequencies;
            }

            TermsEnum termsEnum = termFreqVector.iterator();
            BytesRef text;

            while ((text = termsEnum.next()) != null) {
                String term = text.utf8ToString();
                int freq = (int) termsEnum.totalTermFreq();
                frequencies.put(term, freq);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return frequencies;
    }


    private Map<String, Integer> getAllTermsFrequencies() {
        IndexSearcher searcher = createSearcher(INDEX_PATH + indexName, new BM25Similarity());
        IndexReader reader = searcher.getIndexReader();
        int maxDoc = reader.maxDoc();
        Map<String, Integer> allTermFreqs = new HashMap<>();

        for (int docId = 0; docId < maxDoc; docId++) {
            allTermFreqs.putAll(getDocTermVector(reader, docId));
        }
        return allTermFreqs;
    }

    class DocVectorOld {
        private Map<String, Integer> combinedTerms;
        private Map<String, Integer> v1;
        private Map<String, Integer> v2;

        public DocVectorOld(Map<String, Integer> terms){
            combinedTerms = terms;
            initZeroes();
        }

        private void initZeroes() {
            for (String term: combinedTerms.keySet()) {
                combinedTerms.put(term, 0);
            }
        }

        public Map<String, Integer> getVector(Map<String, Integer> terms){
            Map<String, Integer> vector = new LinkedHashMap<>(combinedTerms);
            for (String term : terms.keySet()) {
                int val = terms.get(term);
                vector.put(term, val);
            }
            return vector;
        }

        public double computeVectors(Map<String, Integer> terms1, Map<String, Integer> terms2){
            assert terms1.size() == terms2.size();
            List<Integer> array1 = new ArrayList<>(terms1.values());
            List<Integer> array2 = new ArrayList<>(terms2.values());
            //List<Double> norm1 = normalize(array1);
            //List<Double> norm2 = normalize(array1);

            double dot = dotProduct(array1, array2);
            double vec1Lenght = vectorLenght(array1);
            double vec2Lenght = vectorLenght(array2);

            return dot / (vec1Lenght * vec2Lenght);
        }

        private List<Double> normalize(List<Integer> list) {
            List<Double> normalized = new ArrayList<>(list.size());
            double vecLen = 0;
            for (int i = 0; i < list.size() - 1; i++) {
                vecLen += Math.pow(list.get(i), 2);
            }
            double norm = Math.sqrt(vecLen);
            list.forEach(freq -> normalized.add(freq/norm));
            return normalized;
        }

        private double dotProduct(List<Integer> array1, List<Integer> array2) {
            assert array1.size() == array2.size();
            double dot = 0;
            for (int i = 0; i < array1.size() - 1; i++) {
                dot += array1.get(i) * array2.get(i);
            }
            return dot;
        }

        private double vectorLenght(List<Integer> list) {
            double vecLen = 0;
            for (int i = 0; i < list.size() - 1; i++) {
                vecLen += Math.pow(list.get(i), 2);
            }
            return Math.sqrt(vecLen);
        }
    }
}

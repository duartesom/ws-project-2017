package diversity;

import net.sf.javaml.clustering.mcl.SparseVector;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

class DocVector{
    private Map<String, Integer> combinedTerms;
    private Map<String, Integer> invertedIndex;

    public DocVector(){
        combinedTerms = new LinkedHashMap<>();
        invertedIndex = new HashMap<>();
    }

    public int getTotalTerms(){
        return combinedTerms.size();
    }

    public void setZeroes() {
        for (String term: combinedTerms.keySet()) {
            combinedTerms.put(term, 0);
        }
    }

    public void addTerms(Map<String, Integer> terms){
        for (String term : terms.keySet()) {
            combinedTerms.put(term, terms.get(term));
        }
    }

    public SparseVector getVector(Map<String, Integer> terms){
        SparseVector vector = new SparseVector(getTotalTerms());
        for (String term : terms.keySet()) {
            double val = (double)terms.get(term);
            vector.put(searchInvertedIndex(term), val);
        }
        return vector;
    }

    public void createInvertedIndex(){
        int index = 0;
        Iterator<String> it = combinedTerms.keySet().iterator();
        while(it.hasNext()){
            String term = it.next();
            invertedIndex.put(term, index);
            index++;
        }
    }

    public int searchInvertedIndex(String term){
        return invertedIndex.get(term);
    }
}
package diversity;

import classes.Util;
import models.InterestProfile;
import models.VectorClass;
import net.sf.javaml.clustering.mcl.SparseVector;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.*;

import static classes.Util.INDEX_PATH;
import static classes.Util.createSearcher;

public class KMeansSimilarity {

    private IndexSearcher searcher;
    private IndexReader reader;
    private List<InterestProfile> profiles;
    private ScoreDoc[] hits;
    private DocVector docVec;
    private int numDoc;

    public KMeansSimilarity(ScoreDoc[] hits) {
        this.hits = hits;
        searcher = createSearcher(INDEX_PATH + "11", new BM25Similarity());
        reader = searcher.getIndexReader();
        profiles = Util.getProfiles();
        numDoc = reader.numDocs();
    }

    public void createDocumentVectors(){
        docVec = new DocVector();

        for (ScoreDoc hit : hits) {
            Map<String, Integer> vector = getDocTerms(hit.doc);
            docVec.addTerms(vector);
        }
        docVec.setZeroes();
        docVec.createInvertedIndex();
    }

    public int getNumTerms(){
        return docVec.getTotalTerms();
    }

    public List<VectorClass> getTfIdfVectors(){
        List<VectorClass> list = new LinkedList<>();
        for (ScoreDoc hit : hits) {
            list.add(calcDocTfIdf(hit.doc));
        }
        return list;
    }

    private VectorClass calcDocTfIdf(int docId) {
        SparseVector vector = null;
        try {
            Map<String, Integer> terms = getDocTerms(docId);
            vector = docVec.getVector(terms);

            for (String term : terms.keySet()) {
                int pos = docVec.searchInvertedIndex(term);
                int freqInDoc   = reader.docFreq(new Term("text", term));
                double idf      = Math.log(numDoc / freqInDoc);
                int tf          = terms.get(term);
                vector.add(pos, tf * idf);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new VectorClass(vector, docId);
    }

    private Map<String, Integer> getDocTerms(int docId) {
        Map<String, Integer> frequencies = new HashMap<>();

        try {
            Terms termFreqVector = reader.getTermVector(docId, "textVector");
            if(termFreqVector == null){
                //System.out.println("no term vector: docId = " + docId);
                return frequencies;
            }

            TermsEnum termsEnum = termFreqVector.iterator();
            BytesRef text;

            while ((text = termsEnum.next()) != null) {
                String term = text.utf8ToString();
                int freq = (int) termsEnum.totalTermFreq();
                frequencies.put(term, freq);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return frequencies;
    }
}

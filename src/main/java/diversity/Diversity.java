package diversity;

import classes.EmailDigester;
import classes.TweetAnalyzer;
import classes.Util;
import models.InterestProfile;
import models.VectorClass;
import models.VectorWrapper;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.BM25Similarity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import static classes.Util.buildQuery;
import static classes.Util.*;

public class Diversity {

    private IndexSearcher searcher;
    private List<InterestProfile> profiles;
    private String indexName;

    public Diversity(String indexName) {
        this.indexName = indexName;
        searcher = createSearcher(INDEX_PATH + indexName, new BM25Similarity());
        profiles = Util.getProfiles();
    }

    public void minHash(){
        MinHashSimilarity min = new MinHashSimilarity(indexName, new TweetAnalyzer());
        EmailDigester digester = new EmailDigester(new TweetAnalyzer());
        createDirectory(MIN_HASH_PATH);

        for (InterestProfile profile : profiles) {
            List<ScoreDoc> docs = min.getProfileDocs(profile);
            try {
                PrintWriter output = outputToFile(MIN_HASH_PATH + profile.getTopid() + ".txt");
                digester.printDigest(output, profile, docs);
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //#TODO: PRINT DOS CLUSTERS
    public void kmeans(){
        for (InterestProfile profile : profiles) {
            Query query = buildQuery(profile.getTitle(), new StandardAnalyzer());
            ScoreDoc[] hits = getSearchDocs(query, searcher);

            KMeansSimilarity ks = new KMeansSimilarity(hits);
            ks.createDocumentVectors();
            List<VectorClass> vectors = ks.getTfIdfVectors();
            List<CentroidCluster> clusters = calcClusters(vectors, 50);
            return;
        }
    }

    private List<CentroidCluster> calcClusters(List<VectorClass> vectors, int k) {
        List<VectorWrapper> clusterInput = new ArrayList<>(vectors.size());
        for (VectorClass vector : vectors)
            clusterInput.add(new VectorWrapper(vector));

        KMeansPlusPlusClusterer clusterer = new KMeansPlusPlusClusterer(k,10000);
        return clusterer.cluster(clusterInput);
    }


}









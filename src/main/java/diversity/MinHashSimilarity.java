package diversity;

import classes.TweetAnalyzer;
import models.InterestProfile;
import models.ShingleWrapper;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.BM25Similarity;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

import static classes.Util.buildQuery;
import static classes.Util.*;

public class MinHashSimilarity {

    public static final int HASH_COUNT = 200;
    private final Analyzer analyzer;
    private IndexSearcher searcher;

    public MinHashSimilarity(String indexName, Analyzer analyzer) {
        this.analyzer = analyzer;
        searcher = createSearcher(INDEX_PATH + indexName, new BM25Similarity());
    }

    public List<ScoreDoc> getProfileDocs(InterestProfile profile) {
        System.out.println("[" + profile.getTopid() + "] -" + "\"" + profile.getTitle() + "\"");
        List<ShingleWrapper> docsShingles = getProfileShingles(profile, 3);
        List<ScoreDoc> finalDocs = docsShingles.stream().map(ShingleWrapper::getDoc).collect(Collectors.toList());

        for (int i = 0; i <= docsShingles.size() - 2; i++) {
            List<Integer> list = docsShingles.get(i).getHashes();
            List<Integer> other = docsShingles.get(i + 1).getHashes();
            double coef = jaccardSimilarity(list, other);

            if (coef > JACCARD_SIMILARITY_TRESHOLD) {
                ScoreDoc doc = docsShingles.get(i).getDoc();
                ScoreDoc doc2 = docsShingles.get(i+1).getDoc();
                finalDocs.removeIf(d -> d.equals(doc));
                System.out.println("Removed doc: " + doc.doc + ", similar to " + doc2.doc + "(" + coef + ")");
            }
        }
        System.out.println("Total docs = " + docsShingles.size());
        System.out.println("Final docs = " + finalDocs.size());
        System.out.println();
        return finalDocs;
    }

    private List<ShingleWrapper> getProfileShingles(InterestProfile profile, int maxShingleSize) {
        List<ShingleWrapper> list = new ArrayList<>();
        Query query = buildQuery(profile.getTitle(), new TweetAnalyzer());
        ScoreDoc[] hits = getSearchDocs(query, searcher);
        IndexReader reader = searcher.getIndexReader();

        for (ScoreDoc hit : hits) {
            Document doc;
            try {
                doc = reader.document(hit.doc);
                String text = doc.get("text");
                List<String> shingles = getDocShingles(text, maxShingleSize);
                List<Integer> hashes = calcShingleHashes(shingles);
                ShingleWrapper wrapper = new ShingleWrapper(hashes, hit);
                list.add(wrapper);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    private double jaccardSimilarity(List<Integer> h1, List<Integer> h2) {
        Set<Integer> intersectSet = new HashSet<>(h1);
        Set<Integer> unionSet = new HashSet<>(h1);
        unionSet.addAll(h2);
        intersectSet.retainAll(h2);
        return (double) intersectSet.size() / unionSet.size();
    }


    private List<String> getDocShingles(String input, int maxShingleSize) {
        List<String> shingles = new LinkedList<>();
        Reader reader = new StringReader(input);
        TokenStream ts = analyzer.tokenStream(null, reader);
        ShingleFilter sf = new ShingleFilter(ts, maxShingleSize);

        try {
            CharTermAttribute cattr = sf.addAttribute(CharTermAttribute.class);
            sf.reset();
            while (sf.incrementToken()) {
                shingles.add(cattr.toString());
            }
            ts.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return shingles;
    }

    private List<Integer> calcShingleHashes(List<String> shingles) {
        List<Integer> hashList = new LinkedList<>();
        for (String shingle : shingles) {
            hashList.add(getMinHash(shingle));
        }
        return hashList;
    }

    private int getMinHash(String shingle) {
        Random rand = new Random(0);
        int minHash = Integer.MAX_VALUE;
        int shingleHash = shingle.hashCode();

        for (int i = 0; i < HASH_COUNT; i++) {
            int hash = shingleHash ^ rand.nextInt();
            if (hash < minHash)
                minHash = hash;
        }
        return minHash;
    }
}

package models;

public class RankDocWrapper implements Comparable<RankDocWrapper> {
    int docId;
    double score;

    public RankDocWrapper(int docId, double score) {
        this.docId = docId;
        this.score = score;
    }

    public int getId() {
        return docId;
    }

    public double getScore() {
        return score;
    }

    @Override
    public int compareTo(RankDocWrapper o) {
        if (score - o.score <= 0)
            return 1;
        else
            return -1;
    }
}

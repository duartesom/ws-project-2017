package models;

import org.apache.lucene.search.ScoreDoc;

import java.util.List;

public class ShingleWrapper {
    private List<Integer> shingleHashes;
    private ScoreDoc doc;

    public ShingleWrapper(List<Integer> shingleHashes, ScoreDoc doc) {
        this.shingleHashes = shingleHashes;
        this.doc = doc;
    }

    public List<Integer> getHashes() {
        return shingleHashes;
    }

    public ScoreDoc getDoc() {
        return doc;
    }
}

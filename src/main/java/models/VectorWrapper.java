package models;

import models.VectorClass;
import net.sf.javaml.clustering.mcl.SparseVector;
import net.sf.javaml.core.DenseInstance;
import net.sf.javaml.core.Instance;
import org.apache.commons.math3.ml.clustering.Clusterable;

import java.util.List;

// wrapper class
public class VectorWrapper implements Clusterable {
    private SparseVector vector;
    private int docId;

    public VectorWrapper(VectorClass vector) {
        this.vector = vector.getVector();
        this.docId = vector.getDocId();
    }

    @Override
    public double[] getPoint() {
        double[] dense = vector.getDense();
        Instance instance = new DenseInstance(dense);
        List<Double> values = (List<Double>) instance.values();
        double[] array = values.stream().mapToDouble(d -> d).toArray();
        return dense;
    }
}
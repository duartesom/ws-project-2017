package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Tweet {
    private final long id;
    private final String text;
    private final String created_at;
    private Calendar calendar;
    private final Map<String , Object> otherProperties = new HashMap<>();

    @JsonCreator
    public Tweet(@JsonProperty("id") long id, @JsonProperty("text") String text, @JsonProperty("created_at") String created_at) {
        this.id = id;
        this.text = text;
        this.created_at = created_at;
        setCalendar();

    }

    private void setCalendar(){
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
            Date date = format.parse(created_at);
            cal.setTime(date);
            calendar = cal;

        } catch (ParseException e) {
            System.err.println("Failed to getProfiles date for created_at: " + "\"" + created_at + "\"");
        }
    }

    public long get_id() {
        return id;
    }

    public String get_text() {
        return text;
    }

    public String get_created_at() {
        return created_at;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public Object get(String name) {
        return otherProperties.get(name);
    }

    @JsonAnyGetter
    public Map<String , Object> any() {
        return otherProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        otherProperties.put(name, value);
    }
}

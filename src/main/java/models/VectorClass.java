package models;

import net.sf.javaml.clustering.mcl.SparseVector;

public class VectorClass {
    private SparseVector vector;
    private int docId;

    public VectorClass(SparseVector vector, int docId) {
        this.vector = vector;
        this.docId = docId;
    }

    public SparseVector getVector() {
        return vector;
    }

    public int getDocId() {
        return docId;
    }
}

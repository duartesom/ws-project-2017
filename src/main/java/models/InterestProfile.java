package models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InterestProfile {

    private final String topid;
    private final String title;
    private final String description;
    private final String narrative;

    @JsonCreator
    public InterestProfile(@JsonProperty("topid") String topid, @JsonProperty("title") String title,
                           @JsonProperty("description") String description, @JsonProperty("narrative") String narrative){
        this.topid = topid;
        this.title = title;
        this.description = description;
        this.narrative = narrative;
    }

    public String getTopid() {
        return topid;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getNarrative() {
        return narrative;
    }
}

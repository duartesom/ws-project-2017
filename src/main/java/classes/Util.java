package classes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.InterestProfile;
import net.sf.javaml.clustering.mcl.SparseVector;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.tools.data.FileHandler;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Util {

    public static final String INDEX_PATH                   = "files/indexes/";
    public static final String DIGESTS_PATH                 = "files/emailDigests/";
    public static final String TWEETS_PATH                  = "files/tweets/rts2016-qrels-tweets2016.jsonl";
    public static final String RTS_PROFILES_PATH            = "files/profiles/TREC2016-RTS-topics.json";
    public static final String MB_PROFILES_PATH             = "files/profiles/TREC2015-MB-eval-topics.json";
    public static final String MIN_HASH_PATH                = "files/MinHash/";
    public static final double JACCARD_SIMILARITY_TRESHOLD  = 0.7;
    public static final int EXPANSION_DOCS                  = 100;

    public static void createDirectory(String path) {
        File dir = new File(path);
        if(!dir.exists())
            dir.mkdir();
    }

    public static void deleteDirectory(File dir) {
        for (File file: dir.listFiles()) {
            if (file.isDirectory()) deleteDirectory(file);
            file.delete();
        }
    }

    public static IndexSearcher createSearcher(String path, Similarity similarity) {
        IndexSearcher searcher = null;
        try {
            IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(path)));
            searcher = new IndexSearcher(reader);
            searcher.setSimilarity(similarity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return searcher;
    }

    public static void closeIndex(IndexSearcher searcher){
        IndexReader reader = searcher.getIndexReader();
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ScoreDoc[] getSearchDocs(Query query, IndexSearcher searcher){
        ScoreDoc[] hits = null;

        try {
            TopDocs results = searcher.search(query, EXPANSION_DOCS);
            hits = results.scoreDocs;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hits;
    }

    public static List<InterestProfile> getProfiles(){
        ObjectMapper mapper = new ObjectMapper();
        List<InterestProfile> profiles = new ArrayList<>();

        try {
            List<InterestProfile> rtsProfiles = mapper.readValue(new File(RTS_PROFILES_PATH), new TypeReference<List<InterestProfile>>(){});
            List<InterestProfile> mbProfiles = mapper.readValue(new File(MB_PROFILES_PATH), new TypeReference<List<InterestProfile>>(){});
            profiles.addAll(rtsProfiles);
            profiles.addAll(mbProfiles);
        } catch (IOException e) {
            System.out.println("Couldn't retreive profiles");
        }
        return profiles;
    }

    public static List<String> parseStopWords() {
        try (BufferedReader br = new BufferedReader(new FileReader("./files/stopWords.txt"))) {
            String line = "";
            List<String> wordsList = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                wordsList.add(line);
            }
            return wordsList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Query buildExpandedQuery(String profile, String expandedTermsString) {
        if(expandedTermsString.equals(""))
            return buildQuery(profile, new StandardAnalyzer());

        QueryParser parser = new QueryParser("text", new StandardAnalyzer());
        BooleanQuery finalQuery = null;

        try {
            Query originalQuery = parser.parse(profile);
            Query expandedQuery = parser.parse(expandedTermsString);

            BoostQuery originalTerms = new BoostQuery(originalQuery, QueryExpansion.ORIGINAL_TERMS_WEIGHT);
            BoostQuery expandedTerms = new BoostQuery(expandedQuery, QueryExpansion.EXPANDED_TERMS_WEIGHT);

            BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
            queryBuilder.add(new BooleanClause(originalTerms, BooleanClause.Occur.MUST));
            queryBuilder.add(new BooleanClause(expandedTerms, BooleanClause.Occur.MUST));
            finalQuery = queryBuilder.build();
        } catch (org.apache.lucene.queryparser.classic.ParseException e) {
            System.out.println("Error parsing query string.");
        }
        return finalQuery;
    }

    public static Query buildQuery(String profile, Analyzer analyzer){
        QueryParser parser = new QueryParser("text", analyzer);
        Query query = null;
        try {
            query = parser.parse(profile);
            return query;
        } catch (org.apache.lucene.queryparser.classic.ParseException e) {
            System.out.println("Error parsing query string.");
        }
        return query;
    }

    public static PrintWriter outputToFile(String path){
        File file = new File(path);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new PrintWriter(writer);
    }


    private void printCSV(List<SparseVector> vectors, String fileName) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < vectors.size(); i++) {
            SparseVector vec = vectors.get(i);
            double[] dense = vec.getDense();
            for (int j = 0; j < dense.length; j++) {
                if(j != dense.length-1)
                    writer.print(dense[j] + ",");
                else
                    writer.print(dense[j]);
            }
            writer.print("\n");
        }
        writer.close();
    }

    private Dataset loadDatasetFromCSV(String path){
        Dataset data = null;
        try {
            data = FileHandler.loadDataset(new File(path), 4, ",");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}

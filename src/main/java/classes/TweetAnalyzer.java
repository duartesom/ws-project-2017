package classes;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.util.IOUtils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author jmag
 *
 */
public class TweetAnalyzer extends Analyzer {

	static List<String> stopWords = Util.parseStopWords();
	static CharArraySet stopSet = new CharArraySet(stopWords, true);

	/** Default maximum allowed token length */
	private int maxTokenLength = 25;

	/**
	 * Builds an analyzer with the default stop words ( ).
	 */
	public TweetAnalyzer() {
	}

	@Override
	protected TokenStreamComponents createComponents(final String fieldName) {
		final StandardTokenizer src = new StandardTokenizer();
		TokenStream tok;

		tok = new StandardFilter(src);					// text into non punctuated text
		tok = new LowerCaseFilter(tok);					// changes all texto into lowercase
		tok = new StopFilter(tok, stopSet);				// removes stop words
        tok = new LengthFilter(tok, 3, Integer.MAX_VALUE);
        tok = new LinkFilter(tok);
		
		return new TokenStreamComponents(src, tok) {
			@Override
			protected void setReader(final Reader reader) {
				src.setMaxTokenLength(TweetAnalyzer.this.maxTokenLength);
				super.setReader(new HTMLStripCharFilter(reader));
			}
		};
	}

	@Override
	protected TokenStream normalize(String fieldName, TokenStream in) {
		TokenStream result = new StandardFilter(in);
		return result;
	}

	// ===============================================
	// Test the different filters
	public static void main(String[] args) throws IOException {

		final String text = "This is a demonstration, of the TokenStream Lucene-API,";
		final String text2 = "http://www.google.pt";

		TweetAnalyzer analyzer = new TweetAnalyzer();
		TokenStream stream = analyzer.tokenStream("field", new StringReader(text));
		List<String> result = new ArrayList<String>();

		Reader filter = new HTMLStripCharFilter(new StringReader(tweet));
		StandardTokenizer ts = new StandardTokenizer();
		final CharTermAttribute termAttribute = ts.addAttribute(CharTermAttribute.class);

		try {
			ts.setReader(filter);
			ts.reset();
			while (ts.incrementToken()) {
				System.out.println(termAttribute.toString());
			}
			ts.end();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.close(ts);
		}

	}

	private class LinkFilter extends TokenFilter {

        private final CharTermAttribute charTermAttribute                   = addAttribute(CharTermAttribute.class);
        private final PositionIncrementAttribute positionIncrementAttribute = addAttribute(PositionIncrementAttribute.class);


        public LinkFilter(TokenStream in) {
            super(in);
        }

        @Override
        public boolean incrementToken() throws IOException {

            // Loop over tokens in the token stream to find the next one
            // that is not empty
            String nextToken = null;
            while (nextToken == null) {

                // Reached the end of the token stream being processed
                if (!this.input.incrementToken()) {
                    return false;
                }

                // Get text of the current token and remove any
                // leading/trailing whitespace.
                String currentTokenInStream = this.input.getAttribute(CharTermAttribute.class).toString().trim();

                // Save the token if it is not an empty string
                if (currentTokenInStream.equals("https")) {
                    return false;
                }
                else{
                    nextToken = currentTokenInStream;
                }
            }

            // Save the current token
            this.charTermAttribute.setEmpty().append(nextToken);
            this.positionIncrementAttribute.setPositionIncrement(1);
            return true;
        }
    }
}

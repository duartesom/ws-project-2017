package classes;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import static classes.Util.EXPANSION_DOCS;
import static classes.Util.getSearchDocs;

public class QueryExpansion {

    private static final int NUMBER_RELEVANT_DOCS = 10;
    private static final int NUMBER_RELEVANT_TERMS = 10;
    public static final float ORIGINAL_TERMS_WEIGHT = 1.0f;
    public static final float EXPANDED_TERMS_WEIGHT = 0.5f;

    public static String getExpansionTerms(String queryString, IndexSearcher searcher, Analyzer analyzer, Similarity similarity) {
        List<Map<String, Integer>> expansionTerms = searchExpansionTerms(queryString, searcher, EXPANSION_DOCS, analyzer, similarity);

        if (expansionTerms == null || expansionTerms.get(0).isEmpty() || expansionTerms.get(1).isEmpty())
            return "";

        List<String> fTerms = getFilteredTerms(expansionTerms);
        if (fTerms.isEmpty())
            return "";
        return fTerms.toString().replaceAll("[,\\[\\]]", " ");
    }

    //TODO : Refatorizar
    private static List<Map<String, Integer>> searchExpansionTerms(String queryString, IndexSearcher searcher, int numExpDocs, Analyzer analyzer, Similarity similarity) {
        searcher.setSimilarity(similarity);
        List<Map<String, Integer>> termsList = new ArrayList<>(2);
        Map<String, Integer> topTerms = new HashMap<>();
        Map<String, Integer> bottomTerms = new HashMap<>();

        try {
            Query query = Util.buildQuery(queryString, new StandardAnalyzer());
            ScoreDoc[] hits = getSearchDocs(query, searcher);
            int numTotalHits = hits.length;
            int numberOfDocs = (numTotalHits < NUMBER_RELEVANT_DOCS) ? numTotalHits : NUMBER_RELEVANT_DOCS;

            // Retrieve most frequent terms in top documents and store them in topTerms
            for (int j = 0; j < numberOfDocs; j++) {
                retrieveTopDocs(searcher.doc(hits[j].doc), analyzer, topTerms);
            }

            int upperLimit = numExpDocs - NUMBER_RELEVANT_DOCS;

            if (numTotalHits < numExpDocs) {
                numExpDocs = numTotalHits;
                upperLimit = 0;
            }

            // Retrieve most frequent terms in bottom documents and store them in botTerms
            for (int j = numExpDocs - 1; j > upperLimit; j--) {
                retrieveBottomDocs(searcher.doc(hits[j].doc), analyzer, bottomTerms);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        termsList.add(topTerms);
        termsList.add(bottomTerms);
        return termsList;
    }

    //TODO : Refatorizar
    private static void retrieveTopDocs(Document doc1, Analyzer analyzer, Map<String, Integer> topTerms) throws IOException {
        String answer = doc1.get("text");
        TokenStream stream = analyzer.tokenStream("field", new StringReader(answer));

        // get the CharTermAttribute from the TokenStream
        CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);

        try {
            stream.reset();

            // print all tokens until stream is exhausted
            while (stream.incrementToken()) {
                String term = termAtt.toString();
                Integer termCount = topTerms.get(term);
                if (termCount == null)
                    topTerms.put(term, 1);
                else
                    topTerms.put(term, ++termCount);
            }

            stream.end();
        } finally {
            stream.close();
        }
    }

    //TODO : Refatorizar
    private static void retrieveBottomDocs(Document doc1, Analyzer analyzer, Map<String, Integer> bottomTerms) throws IOException {
        String answer = doc1.get("text");

        TokenStream stream = analyzer.tokenStream("field", new StringReader(answer));

        // get the CharTermAttribute from the TokenStream
        CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);

        try {
            stream.reset();

            // print all tokens until stream is exhausted
            while (stream.incrementToken()) {
                String term = termAtt.toString();
                Integer termCount = bottomTerms.get(term);
                if (termCount == null)
                    bottomTerms.put(term, 1);
                else
                    bottomTerms.put(term, ++termCount);
            }

            stream.end();
        } finally {
            stream.close();
        }
    }

    private static List<String> sortMap(Map<String, Integer> map) {
        Object[] mapArray = map.entrySet().toArray();
        Arrays.sort(mapArray, (Comparator) (o1, o2) -> ((Map.Entry<String, Integer>) o2).getValue()
                .compareTo(((Map.Entry<String, Integer>) o1).getValue()));

        List<String> mostFreqWords = new ArrayList<>();
        int numberOfTerms = (mapArray.length < NUMBER_RELEVANT_TERMS) ? mapArray.length : NUMBER_RELEVANT_TERMS;

        for (int i = 0; i < numberOfTerms; i++) {
            mostFreqWords.add(((Map.Entry<String, Integer>) mapArray[i]).getKey());
        }
        return mostFreqWords;
    }

    private static List<String> getFilteredTerms(List<Map<String, Integer>> expansionTerms) {
        List<String> topFreqWords = sortMap(expansionTerms.get(0));
        List<String> bottomFreqWords = sortMap(expansionTerms.get(1));
        topFreqWords.removeAll(bottomFreqWords);
        return topFreqWords;
    }
}

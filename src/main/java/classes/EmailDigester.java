package classes;

import models.InterestProfile;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.List;

import static classes.QueryExpansion.*;
import static classes.Util.*;

public class EmailDigester {

    private Analyzer analyzer;
    private IndexSearcher searcher;
    private Similarity similarity;
    private List<InterestProfile> profiles;

    public EmailDigester(Analyzer analyzer) {
        this.analyzer = analyzer;
    }

    /**
     * Itera sobre a lista dos indices diários de tweets e cria os email digests
     * @param similarity Retreival model escolhido para a procura de documentos relevantes no indice de tweets
     */
    public void createEmailDigests(Similarity similarity) {
        profiles = Util.getProfiles();
        this.similarity = similarity;
        Directory indexDir;
        try {
            indexDir = FSDirectory.open(Paths.get(INDEX_PATH));
            String[] indexNames = indexDir.listAll();
            for (int i = 0; i < indexNames.length - 1; i++) {
                indexSearch(indexNames[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cria a pasta onde vão ser guardados os email digests, cria uma instancia do Index Searcher para o indice
     * criado dado o seu nome e cria dois conjuntos de emails digests usando query expansion e sem query expansion.
     * @param indexName O nome do indice que corresponde aos tweets num determinado dia
     */
    private void indexSearch(String indexName){
        createDirectory(DIGESTS_PATH);
        searcher = createSearcher(INDEX_PATH + indexName, similarity);
        buildProfileTweets(indexName, true);
        buildProfileTweets(indexName, false);
        closeIndex(searcher);
    }


    /**
     * Cria uma pasta com o nome do retreival model utilizado e itera sobre a lista dos perfis de interesse:
     * No caso em que se utilize query expension obtem os termos mais comuns e constroi uma query com o termo
     * original(o titulo do perfil de interesse) e os termos obtidos pela expansao. Sem expansao cria apenas
     * uma query com o nome do perfil de interesse. Imprime os documentos mais relevantes num ficheiro de texto
     * indicando o dia do indice e se foi utilizada query expansion ou nao.
     * @param indexName O nome do indice que corresponde aos tweets num determinado dia
     * @param expand True para usar query expansion, False para query simples
     */
    private void buildProfileTweets(String indexName, boolean expand) {
        String path = DIGESTS_PATH + similarity.getClass().getSimpleName();
        createDirectory(path);
        path += expand ? "/" + indexName + "_digest_expanded.txt" : "/" + indexName + "_digest.txt";
        PrintWriter output = outputToFile(path);

        System.out.format("Creating email digests of index %s, with %s, expanded: %b \n", indexName,
                similarity.getClass().getSimpleName(), expand);

        if (expand) {
            for (InterestProfile profile : profiles) {
                String expandedTermsString = getExpansionTerms(profile.getTitle(), searcher, analyzer, similarity);
                Query query = buildExpandedQuery(profile.getTitle(), expandedTermsString);
                ScoreDoc[] hits = getSearchDocs(query, searcher);
                printDigest(output, profile, hits);
            }
        } else {
            for (InterestProfile profile : profiles) {
                Query query = buildQuery(profile.getTitle(), analyzer);
                ScoreDoc[] hits = getSearchDocs(query, searcher);
                printDigest(output, profile, hits);
            }
        }
        output.close();
    }

    /**
     * Imprime a lista de tweets consoante o formato necessario para
     * a avaliaçao posterior utilizando o script de python fornecido
     * @param output ficheiro onde sera imprimida a lista de tweets
     * @param profile nome do interest profile
     * @param hits lista de tweets a imprimir
     */
    private void printDigest(PrintWriter output, InterestProfile profile, ScoreDoc[] hits) {
        try{
            for (int j = 0; j < hits.length; j++) {
                Document doc = searcher.doc(hits[j].doc);
                String id = doc.getField("id").stringValue();
                String formattedDate = doc.getField("formatted_date").stringValue();
                String line = String.format("%s %s %s %s %d %f %s", formattedDate, profile.getTopid(),
                        "Q0", id, j + 1, hits[j].score, "Run-ID");
                output.println(line);
            }
        }catch(IOException e){
            System.err.println("Documento nao encontrado no indice");
        }

    }

    public void printDigest(PrintWriter output, InterestProfile profile, List<ScoreDoc> hits) throws IOException {
        ScoreDoc[] array = hits.toArray(new ScoreDoc[hits.size()]);
        printDigest(output, profile, array);
    }
}

package classes;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Tweet;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static classes.Util.*;

class Indexer {

    public Indexer(boolean clearIndex){
        init(clearIndex);
    }

    public Indexer(boolean clearIndex, Similarity similarity, Analyzer analyzer){
        this.analyzer = analyzer;
        this.similarity = similarity;
        init(clearIndex);
    }

    private IndexWriter currentIndex    = null;
    private Directory previousIndex     = null;
    private Similarity similarity       = new BM25Similarity();
    private Analyzer analyzer           = new StandardAnalyzer();

    private void init(boolean clearIndex){
        createDirectory(INDEX_PATH);
        File indexes = new File(INDEX_PATH);

        if(clearIndex) {
            System.out.println("Cleared index directories.");
            for (File dir : indexes.listFiles()) deleteDirectory(dir);
            createIndexes(similarity, analyzer);
        }
        if(indexes.list().length == 0)
            createIndexes(similarity, analyzer);
    }

    private void createIndexes(Similarity similarity, Analyzer analyzer) {
        Map<Integer,List<Tweet>> tweetsByDayMap = parseTweetDataset();

        //Transformar o mapa dos tweets num array para ser iterado mantendo uma variavel de indice
        Set<Map.Entry<Integer,List<Tweet>>> set = tweetsByDayMap.entrySet();
        List<Map.Entry<Integer,List<Tweet>>> array = new ArrayList<>();
        array.addAll(set);

        for(int index = 0; index < array.size()-1; index++){
            int day = array.get(index).getKey();
            List<Tweet> tweets = array.get(index).getValue();
            System.out.format("Creating index of day %d with %d tweets \n", day, tweets.size());
            openIndex(analyzer, similarity, String.valueOf(day));

            for(Tweet tweet : tweets)
                indexTweet(tweet);
            closeIndex();
        }
    }

    private static Map<Integer,List<Tweet>> parseTweetDataset() {
        System.out.println("Parsing tweet list...");
        // ====================================================
        // Parse the tweet dataset

        Map<Integer,List<Tweet>> tweetsByDayMap = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(TWEETS_PATH))) {
            String line;
            line = br.readLine();

            // ====================================================
            // Para cada entrada do JSON de tweets cria um objeto Tweet para ser guardado no mapa com listas de tweets por dia
            while (line != null) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                Tweet tweet = mapper.readValue(line, Tweet.class);
                int day = tweet.getCalendar().get(Calendar.DAY_OF_MONTH);

                //Cria nova lista no mapa se ainda nao existirem tweets desse dia,
                // caso contrario adiciona o tweet a lista de tweets desse dia
                if(!tweetsByDayMap.containsKey(day)){
                    List<Tweet> list = new ArrayList<>();
                    list.add(tweet);
                    tweetsByDayMap.put(day,list);
                }
                else{
                    tweetsByDayMap.get(day).add(tweet);
                }
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
        return tweetsByDayMap;
    }

    private void indexTweet(Tweet tweet) {

        Document doc = new Document();
        long id = 0;

        try {
            // Extract field id
            id = tweet.get_id();
            doc.add(new LongPoint("id",id));
            doc.add(new StoredField("id",id));

            // Extract field text
            String text = tweet.get_text();
            doc.add(new TextField("text", text, Field.Store.YES));

            // Store term vectors
            FieldType type = new FieldType();
            type.setStoreTermVectors(true);
            type.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
            doc.add(new Field("textVector",text, type));

            // Extract field created_at
            Calendar cal = tweet.getCalendar();
            Date date = cal.getTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            String formattedDate = format.format(date);

            long time = date.getTime();
            doc.add(new LongPoint("created_at", time));
            doc.add(new StoredField("created_at", time));
            doc.add(new StoredField("formatted_date", formattedDate));

            currentIndex.addDocument(doc);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openIndex(Analyzer analyzer, Similarity similarity, String indexName) {
        try {
            // size to the JVM (eg add -Xmx512m or -Xmx1g):
            // iwc.setRAMBufferSizeMB(256.0);
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            Directory dir = FSDirectory.open(Paths.get(INDEX_PATH + indexName));
            iwc.setSimilarity(similarity);
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            currentIndex = new IndexWriter(dir, iwc);

            // Verifica se a diretoria nao esta vazia e preenche o novo index com os documentos do dia anterior
            if(previousIndex != null){
                if(previousIndex.listAll().length > 1)
                    currentIndex.addIndexes(previousIndex);
            }
            previousIndex = dir;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void closeIndex() {
        try {
            currentIndex.close();
        } catch (IOException e) {
            System.out.println("Error closing the index.");
        }
    }
}

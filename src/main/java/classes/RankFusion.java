package classes;

import models.InterestProfile;
import models.RankDocWrapper;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.Similarity;

import java.util.*;

import static classes.Util.*;

public class RankFusion {

    private List<Similarity> similarities;
    private List<List<ScoreDoc>> docsList;
    private Set<Integer> allDocsId;
    private String indexName;
    private Analyzer analyzer;
    private static final int ATTENUATION_FACTOR = 60;

    public RankFusion(String indexName, Analyzer analyzer) {
        allDocsId = new HashSet<>();
        similarities = new LinkedList<>();
        docsList = new ArrayList<>();
        this.indexName = indexName;
        this.analyzer = analyzer;
    }

    public List<RankDocWrapper> reciprocalRank(InterestProfile profile) {
        if (similarities.isEmpty()) {
            System.err.println("Add similarities to RankFusion");
            return null;
        } else {
            buildDocRanks(profile);
            groupAllDocs();
            return calcRankings();
        }
    }

    public void addSimilarity(Similarity... similarity) {
        similarities.addAll(Arrays.asList(similarity));
    }

    public void buildDocRanks(InterestProfile profile) {
        for (Similarity similarity : similarities) {
            IndexSearcher searcher = createSearcher(INDEX_PATH + indexName, similarity);
            String expansionTerms = QueryExpansion.getExpansionTerms(profile.getTitle(), searcher, analyzer, similarity);
            Query query = buildExpandedQuery(profile.getTitle(), expansionTerms);
            ScoreDoc[] docs = getSearchDocs(query, searcher);
            List<ScoreDoc> list = Arrays.asList(docs);
            docsList.add(list);
        }
    }

    private void groupAllDocs() {
        for (List<ScoreDoc> docs : docsList) {
            for (ScoreDoc doc : docs) {
                allDocsId.add(doc.doc);
            }
        }
    }

    private List<RankDocWrapper> calcRankings() {
        List<RankDocWrapper> finalRanking = new ArrayList<>();
        for (int docId : allDocsId) {
            List<Integer> positions = findPositions(docId);
            double score = RRFScore(positions);
            RankDocWrapper rankDoc = new RankDocWrapper(docId, score);
            finalRanking.add(rankDoc);
        }
        Collections.sort(finalRanking);
        return finalRanking;
    }

    private List<Integer> findPositions(int docId) {
        List<Integer> positions = new LinkedList<>();
        for (List<ScoreDoc> docs : docsList) {
            int position = indexOf(docs, docId);
            positions.add(position + 1);
        }
        return positions;
    }

    private double RRFScore(List<Integer> positions) {
        double score = 0.0;
        for (int position : positions) {
            if (position == 0)
                continue;
            score += 1.0 / (ATTENUATION_FACTOR + position);
        }
        return score;
    }

    private int indexOf(List<ScoreDoc> docs, int docId) {
        int index = 0;
        for (int i = 0; i < docs.size() - 1; i++) {
            if (docs.get(i).doc == docId) {
                return i;
            }
        }
        return index;
    }

}



























package classes;

import diversity.Diversity;
import models.InterestProfile;
import models.RankDocWrapper;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;

import java.util.Collections;
import java.util.List;

class Main {
    Analyzer analyzer = new TweetAnalyzer();

    public static void main(String[] args) {
        Main m = new Main();
        //m.testEmailDigests();
        //m.testRankFusion();
        m.testDiversity();
    }

    private void testRankFusion(){
        RankFusion rf = new RankFusion("11", analyzer);
        rf.addSimilarity(new BM25Similarity(), new ClassicSimilarity(), new LMDirichletSimilarity());
        List<InterestProfile> profiles = Util.getProfiles();
        List<RankDocWrapper> rankedDocs = rf.reciprocalRank(profiles.get(0));
    }

    private void testEmailDigests(){
        //new Indexer(true, new BM25Similarity(), new TweetAnalyzer());
        EmailDigester digester = new EmailDigester(analyzer);
        digester.createEmailDigests(new LMDirichletSimilarity());
        digester.createEmailDigests(new ClassicSimilarity());
        digester.createEmailDigests(new BM25Similarity());
    }

    private void testDiversity(){
        Diversity div = new Diversity("12");
        div.minHash();
        //div.kmeans();
    }
}

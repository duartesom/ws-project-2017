[Link do guia do projeto](http://ctp.di.fct.unl.pt/~jmag/ws/materials/WS_project.pdf)

#1. Daily digest:

* Implement an incremental indexing where you store a separate index for each day. 
	Each index contains the Tweets of that day and all previous days. ✓

* **Baseline 1 [5%]**: Build the daily email digest as a retrieval task with the LMD retrieval model. 
	Calibrate the μ parameter. ✓

* **Baseline 2 [5%]**: Build the daily email digest as a retrieval task with the BM25 retrieval
	model. Calibrate the b and k1 parameters. ✓

* **Baseline 3 [20%]**: Use pseudo-relevance feedback to expand the initial query and
	improve the daily summaries. Justify the selection of the retrieval models. Calibrate the
	parameters. ✓

* **Evaluation [5%]**: Compare and analyse the performance of the different retrieval models.

#2. Diverse daily digests:

* **Baseline 4 [15%]**: To address the problem of information diversity, i.e. groups of tweets
	that are too similar in content, implement a strategy with KMeans to remove redundant
	tweets from your search results.

* **Baseline 5 [15%]**: Following the MinHash algorithm, implement a near-duplicate
	detection method with the Jaccard coefficient to remove redundant tweets from your
	search results.

* **Evaluation [15%]**: Do a success and failure analysis of your results. Identify the elements
	in your system that contribute positively and negatively. Discuss your insights.

#3. Rank fusion:

* **Combining multiple fields [10%]**: using the reciprocal rank fusion method, implement a
	search model to combine multiple ranking models.
	Evaluation [10%]: Compare and analyse the performance of your system with multiple
	fields.

#4. Discussion of possible improvements [5%]